# Install and Configure VI for PowerShell

PowerShell VI is a solution to quickly install and configure gvim on Windows to integrate with PowerShell console giving a single window for running code and editing files. Installation supports a single command install which will download the gvim80.exe installation file, run the installer, then setup environment configurations for your profile.

To install and run the configuration copy and paste the following command into PowerShell.

```powershell
iex (New-Object Net.WebClient).DownloadString('https://gitlab.com/rolston/psvi/raw/master/invoke-psviinstall.ps1')
```
