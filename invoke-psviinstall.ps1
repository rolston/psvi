﻿
# script setup pulled from http://codeandkeep.com/PowerShell-And-Vim/
# works great and much appreciated!

if((Get-Module PSReadline).Version -LT 1.2) {
  write-host "PSReadline being updated to latest version"
  Install-Package -Name PSReadline -Force 
}
write-host "setting PSReadline Option edit mode to vi"
Set-PSReadlineOption -EditMode vi -BellStyle None

write-host "checking if vim already installed"
if(!(Test-Path 'C:\Program Files (x86)\Vim\vim80')){
  write-host "vim80 not found. Downloading installation now.."
  # download and run the installer
  $installer = "$env:TEMP\gvim80-586.exe"
  Invoke-WebRequest -Uri 'https://ftp.nluug.nl/pub/vim/pc/gvim80-586.exe' -OutFile $installer
  write-host "vim installation download complete. Now running installation .exe file"
  & $installer
}
write-host "vim installed. now configuring vim for PowerShell..."
$ProfileString = @'
# Add these lines to your $PROFILE
New-Alias -Name vi -Value 'C:\Program Files (x86)\vim\vim80\vim.exe'
New-Alias -Name vim -Value 'C:\Program Files (x86)\vim\vim80\vim.exe'

# Include this if you like a vim command line experience
Set-PSReadlineOption -EditMode vi -BellStyle None
'@

if(Test-Path $profile){
  write-host "Profile file exists, checking if configured for vi"
  $profileContent = Get-Content $profile
  if($profile -contains $ProfileString) {
    # we are all set
    write-host "Profile already set for vi alias"
  }
  else {
    # add the following to the file:
    $ProfileString | Out-File $profile -Append
    write-host "Profile updated to support vi in PowerShell"
  }
}
else {
  # the file did not exist so lets create it
  write-host "Profile file did not exist. Creating file and configuring it for vi in PowerShell"
  $ProfileString | Out-File $profile
}
write-host "Profile configuration complete."

$vimrcFile = @'
" Enable syntax highlighting
syntax on

" Change tabs to 2 spaces for efficiency
set expandtab 
set tabstop=2
set shiftwidth=2

" Automatically indent when starting new lines in code blocks
set autoindent

" Add line numbers
set number

" shows column, & line number in bottom right 
set ruler

" Color to set; however I liked the default. Uncomment and change below if you want
" colorscheme shine

" helpful if using 'set ruler' and 'colorscheme shine', makes lineNumbers grey
" Same example from http://vim.wikia.com/wiki/Display_line_numbers
highlight LineNr term=bold cterm=NONE ctermfg=DarkGrey ctermbg=NONE gui=NONE guifg=DarkGrey guibg=NONE

" Disable bell sounds 
set noerrorbells visualbell t_vb=
'@

$vimrc="~\_vimrc"
write-host "Configuring vimrc file for PowerShell"
# Make it if it's not already there
if(!(Test-Path -Path $vimrc)){
  Write-Host "$vimrc not found. Creating file and adding configurations"
  $vimrcFile | Out-File $vimrc -Encoding utf8
}
else {
  $vimrc = Get-Content $vimrc
  if($vimrc -contains $vimrcFile){
    write-host "$vimrc file already configured"
  }
  else {
    # backup the old file
    write-host "$vimrc foud, but configuration not set. Backing up current file to $vimrc.bak"
    $vimrc | Out-file -FilePath "$vimrc.bak"
    write-host "vimrc file back up complete."
    $vimrcFile | Out-File $vimrc -Encoding utf8
    write-host "new configurations set to $vimrc file."
  }
}